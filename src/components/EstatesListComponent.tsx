//= Functions & Modules
// Others
import { useImperativeHandle, forwardRef } from 'react';

export enum EstateListViews {
    GRID = 0,
    LIST = 1,
}

type Props = {
    view: EstateListViews;
};

/**
 * @component
 *
 * @param {EstateListViews} view
 */
const EstatesListComponent = forwardRef((props: Props = { view: EstateListViews.GRID }, ref) => {
    async function search(searchData: any) {
        // TODO: To be implemented
    }

    // Expose methods
    useImperativeHandle(ref, () => ({
        search,
    }));

    return null;
});

export default EstatesListComponent;
