//= Functions & Modules
//
import { useTranslations } from 'next-intl';

export function formatCharacteristic(
    characteristicName: string,
    characteristicValue: any,
    characteristicsTranslations: ReturnType<typeof useTranslations>
): string {
    if (characteristicValue == null) return '';

    if (characteristicName === 'nrcamere') {
        return `${characteristicValue} ${characteristicsTranslations('nrcamere')}`;
    }

    return 'error';
}
