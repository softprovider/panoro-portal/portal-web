//= Structures & Data
// Own
import { URLEstateTransactionTypes } from 'src/data/URLEstateTransactionTypes';
// Others
import { EstateTransactionTypes } from '@softprovider/core-reactjs-items';

const URLtoEstateTransactionTypes = {
    [URLEstateTransactionTypes[EstateTransactionTypes.VANZARE]]: EstateTransactionTypes.VANZARE,
    [URLEstateTransactionTypes[EstateTransactionTypes.INCHIRIERE]]: EstateTransactionTypes.INCHIRIERE,
    [URLEstateTransactionTypes[EstateTransactionTypes.REGIM_HOTELIER]]: EstateTransactionTypes.REGIM_HOTELIER,
};

export default function resolveURLEstateTransactionTypes(urlTransactionType: string): EstateTransactionTypes {
    return URLtoEstateTransactionTypes[urlTransactionType];
}
