//= Structures & Data
// Own
import { URLEstateCategories } from '../data/URLEstateCategories';
// Others
import { EstateCategories } from '@softprovider/core-reactjs-items';

const URLtoEstateCategory = {
    [URLEstateCategories[EstateCategories.APARTAMENTE]]: EstateCategories.APARTAMENTE,
    [URLEstateCategories[EstateCategories.CASE]]: EstateCategories.CASE,
    [URLEstateCategories[EstateCategories.BIROURI]]: EstateCategories.BIROURI,
    [URLEstateCategories[EstateCategories.TERENURI]]: EstateCategories.TERENURI,
    [URLEstateCategories[EstateCategories.SPATII_COMERCIALE]]: EstateCategories.SPATII_COMERCIALE,
    [URLEstateCategories[EstateCategories.SPATII_INDUSTRIALE]]: EstateCategories.SPATII_INDUSTRIALE,
};

export default function resolveURLEstateCategory(urlCategory: string): EstateCategories {
    return URLtoEstateCategory[urlCategory];
}
