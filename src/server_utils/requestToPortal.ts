export default async (method: 'POST' | 'GET', path: string, data?: { [key: string]: any }) => {
    let url: string = `${process.env.PORTAL_URL}${path}`;
    let options: RequestInit = {
        method,
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'x-token': process.env.PORTAL_TOKEN,
        },
        mode: 'cors',
    };

    if (data) {
        if (method === 'GET') {
            const queryParams = new URLSearchParams();
            for (const key in data) {
                queryParams.set(key, data[key]);
            }

            url += `?${queryParams.toString()}`;
        } else if (method === 'POST') {
            options.body = JSON.stringify(data);
        }
    }

    console.log(`CALLING ${options.method}:"${url}" and data: "${options.body}"`);

    try {
        const request = await fetch(url, options);
        console.log('request', request);

        return request.json();
    } catch (error) {
        console.error(error);
    }
};
