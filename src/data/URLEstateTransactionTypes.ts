//= Structures & Data
import { EstateTransactionTypes } from '@softprovider/core-reactjs-items';

export const URLEstateTransactionTypes = {
    [EstateTransactionTypes.VANZARE]: 'vanzari',
    [EstateTransactionTypes.INCHIRIERE]: 'inchiriere',
    [EstateTransactionTypes.REGIM_HOTELIER]: 'regim-hotelier',
};
