export type SortItem = {
    label: string;
    data: { [key: string]: 1 | -1 };
};
