export type SearchEventDetail = {
    from: string;
    urlName: string;
    fieldName: string;
    data: any;
};
