//= Structures & Data
// Shared
import { EstateCategories } from '@softprovider/core-reactjs-items';

export default {
    [EstateCategories.APARTAMENTE]: ['suprafataUtila', 'nrcamere', 'parcare', 'etaj', 'bai', 'anConstructie'],
    [EstateCategories.CASE]: ['suprafataUtila', 'nrcamere', 'bai', 'anConstructie', 'suprafataTeren'],
    [EstateCategories.BIROURI]: ['suprafataUtila', 'nrIncaperi', 'bai', 'clasaBirouri'],
    [EstateCategories.SPATII_COMERCIALE]: ['suprafataUtila', 'nrIncaperi'],
    [EstateCategories.SPATII_INDUSTRIALE]: ['suprafataUtila', 'inaltimeSpatiu', 'suprafataTeren'],
    [EstateCategories.TERENURI]: ['suprafataTeren', 'frontStradal', 'curent', 'apa', 'gaz', 'canalizare'],
};
