//= Structures & Data
// Others
import { EstateCategories } from '@softprovider/core-reactjs-items';

export const URLEstateCategories = {
    [EstateCategories.APARTAMENTE]: 'apartamente',
    [EstateCategories.CASE]: 'case',
    [EstateCategories.BIROURI]: 'birouri',
    [EstateCategories.TERENURI]: 'terenuri',
    [EstateCategories.SPATII_COMERCIALE]: 'spatii-comerciale',
    [EstateCategories.SPATII_INDUSTRIALE]: 'spatii-industriale',
};
