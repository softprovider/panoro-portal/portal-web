//= Structures & Data
import { GetStaticProps } from 'next';

import Head from 'next/head';
import Image from 'next/image';

import requestToPortal from '../server_utils/requestToPortal';

export default function Home() {
    return (
        <>
            <Head>
                <title>Create Next App</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <div className="flex justify-center">
                <h1>WORKS</h1>
            </div>
        </>
    );
}

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    console.log('AAAAAAAAAAA', locale);
    console.log('WWW', await requestToPortal('POST', '/api/v2/estate_categories/get/all'));

    return {
        props: {
            messages: require(`../locales/${locale}`).default,
        },
    };
};
