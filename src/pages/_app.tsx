//= Functions & Modules
import { NextIntlProvider } from 'next-intl';

//= Style & Assets
import '../styles/styles.css';

export default function MyApp({ Component, pageProps }) {
    console.log('locale', pageProps.messages);
    return (
        <NextIntlProvider messages={pageProps.messages}>
            <Component {...pageProps} />
        </NextIntlProvider>
    );
}
