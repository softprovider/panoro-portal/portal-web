# Panoro.ro Web

## Installing packages

Installing all the required packages you requires the GitLab Auth Token then
call the next command by replace `{GITLAB_AUTH_TOKEN}` with the given token:

```bash
npm i && GITLAB_AUTH_TOKEN={GITLAB_AUTH_TOKEN} npm run-script installExtra
```

## Before running

Before running the application create a file named `.env.local` with the
following data:

```
PORTAL_URL={PORTAL_URL}
PORTAL_TOKEN={PORTAL_TOKEN}
```

The `{PORTAL_URL}` is the URL to the portal's service. Must be a fully
qualified URL and not ending in a dash (`/`).

The `{PORTAL_TOKEN}` is the token required for the portal's service to allow us
to request data.

## How to run

To start the the development server run:

```bash
npm run dev
```

## Style guide

The project is using Prettier for formatting the files.

The project is using raw functions for the ReactJS components and not React.FC directive.

### Includes

The `includes` lines should be grouped in the following way:

```js
//= Functions & Modules
// Own
all the imports that are functions/classes/etc that are created by/for this project
// Others
all the imports that are functions/classes/etc from the packages/node_modules

//= Structures & Data
// Own
all the imports that are simple objects/enums that are created by/for this project
// Others
all the imports that are simple objects/enums from the packages/node_modules

//= React components
// Own
all the imports that are React components that are created by/for this project
// Others
all the imports that are React components from the packages/node_modules

//= Style & Assets
// Own
all the imports that are style (css files) and assets (images) that are created by/for this project
// Others
all the imports that are style (css files) and assets (images) from the packages/node_modules
```

## Localization

The application is using `next-intl` for localization which uses `i18n` implementation of Next.js.

The localization data is stored in `locales` directory, each language stored in its own directory.

## Client utilities

## Server utilities

The function `src/server_utils/requestToPortal.ts` is able to make calls to the
portal's service.

## Files structures

See `src/docs/res/ProjectFilesStructure.md`.

## Pages

The pages of the projects are:

-   **Home page**
-   **Search page** - read the documentation of the page from `docs/res/SearchPage.md`
