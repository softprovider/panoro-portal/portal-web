# Estates list component

> File /src/components/EstatesListComponent.tsx

The estates list component is the component that displays a list of estates in
a programmable order (view or list) that is used in all the pages of the
Panoro.ro portal.

## Estate item componet

The estate list item component is the estate component that is used in the list
of estates displayed in all the pages.

It consists of displaying the most critical data that a user should be 
interested in. These items are:

-   the cover photo;
-   the price (and if there is VAT included in it);
-   the title;
-   the location;
-   critical characteristics;
-   having or not a VR/360 tour;
-   the name of the user and the type of user who sells it;

### The critical characteristics

The critical characteristics are formed of an icon and a text.

They are different depending on the category of the estate.

The fields of the critical characteristics can be found in
`src/data/EstateCriticalCharacteristics.ts` where they are organized in an
object where each key is the category of the estate (from 
`@softprovider/panoro-core-reactjs-items`) and the value is an array of strings
representing the name of the field.

Each field in the array has the value stored in the `characteristics` object
of each estate data.

The value is transformed using the `src/client_utils/formatCharacteristic.ts` 
for displaying a human readable value.

The icon for each characteristic are stored in the directory 
`public/imgs/characteristics` where each image is in SVG file where the name
of the file is the characteristic name.

The icons are displayed using `next/image` for speed and optimization required
for better SEO results.