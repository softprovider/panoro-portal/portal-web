# File structure

-   `public` - the public directory for the website
-   `src` - source files directory
    -   `components` - all React components (that are not pages)
        -   `[page_name]` - each specific components should be a directory named after the page name
    -   `pages` - all the pages of the website
        -   `api` - the server requests
    -   `server_utils` - all utilities related to the server/backend
    -   `client_utils` - all utilities related to the client/frontend
    -   `locale` - the locale files

Rules for naming the files and directories:

-   directories should be in **snake_case**
-   React components should be in **PascalCase**
-   function files should be in **camelCase**
