# Search page

## Diagrams

-   User flow (with client details) - `docs/res/SearchPage_UserFlow_Tehnical.svg`
    (source file is `docs/res/SearchPage_UserFlow_Tehnical.puml`)

## URL

The URL of the search page is of the following form:

```
/cauta/{urlTransactionType}/{urCategory}/{countyEncodedName}/{cityEncodedName}/{neighborhoodEncodedName}/{pageIndex}
```

The URL consists of the following path parameters:

-   **urlTransactionType** - the URL name of the transaction type (see
    `src/data/URLEstateTransactionTypes.ts`)
-   **urlCategory** - the URL name of the category (see
    `src/data/URLEstateCategories.ts`);
-   **countyEncodedName** - the encoded name of the county (location of
    `level=1`);
-   **cityEncodedName** - the encoded name of the city (location of `level=2`);
-   **neighborhoodEncodedName** _not always present_ - the encoded name of one
    neighborhood (location of `level=3`) or more neighborhoods separated by
    comma (`,`); **neighborhoodEncodedName** can be missing;
-   **pageIndex** _not always present_ - the index of the page now displayed;
    **pageIndex** can be missing;

> **pageIndex** is added into the URL as a URL parameter and not as a query
> parameter because NextJS can only include the URL parameters when statically
> generating the paths and cannot include the query parameters, thus, for SEO
> optimizations concerns we included the index of the page as an URL parameter.

The query part consists of the following:

-   selected filter options;
-   `sort={sortIndex}` for the selected sort option (if any beside `default`);
-   `list` if the selected view is `list`;

## Initialization

The initialization phase is done only once when the page is loaded and consists
of the followings steps:

-   [resolve the URL](#resolving-the-url);
-   [getting the values for \<head\>](#html-head);
-   [perform the initial search for estates](#initial-search);

All the initialization is done in the `getStaticProps` for optimization and
speed.

### Resolving the URL

Resolving the URL results in getting the locations IDs, resolving the
estate transaction type, resolving the estate category, getting the range of
estates to get from page index and parsing the query parameters where the filter
options and sort option are set.

Resolving the estate category is done by calling
`src/client_utils/resolveURLEstateCategory` and resolving estate transaction
types is done by calling `src/client_utils/resolveURLEstateTransactionTypes`.

Getting the IDs of the locations that are present in the URL is done by calling
`/api/v2/locations/get/by_url_name` with the location encoded value (the value
from the URL). See [#Locations].

From the page index the range of how many estates to get is calculated. See
[#Pagination].

Parsing the query parameters and saving the filter and sort options for the
initial search, but also to be later, when the client renders the page, to set
the filter and sort components values.

### HTML Head

In the initialization step that happens in `getStaticProps` we also get from the
portal's service the data that should do in the \<head\>. Calling
`/api/v2/portal/search_page/get/head` returns a string that contains ReactJS syntax that is converted to
ReactJS components using `html-react-parser`.

### Initial search

After all these data are retrieved/calculated we construct the search data from
them for an initial search for the estates.

By getting the required estates in `getStaticProps` we can then statically
generate all the pages when building the app with Next.js.

### Getting non-essential data

The following actions are considered non-essential data because are not
required for initial drawing of the page:

-   getting other filter characteristics (see [#Filtering system]);
-   getting all the city's neighborhoods;
-   getting the advertisement banners;

The non-essential data is loaded after the page was rendered.

## Generating all the possible paths

Generating all the possible paths using Next.js's `getStaticPaths` is usefull
for generating the static pages when building the application.

All the locations are requested by calling `/api/v2/locations/get/all_children`
firstly with the location ID of Romania (from `src/data/RomaniaCountryID`) thus
getting all the counties (location type 1). For each county calling the same
request, but now with the county ID and getting all the cities for each county
and creating all the URLs for each city of each county for each estate
transaction type and for each estate category.

Repeating the same step for each city we get all the neighborhoods and creating
all the URLs for all the locations of type 3.

For each city and for each neighborhood we get the count of estates each
location has by requestion `/api/v2/estates/get/many` with the `count` property
set to `1` and setting the URL for each page.

## Search system

The search system is using events for signaling changes. The name of the event
is retrieved from `src/data/search_page/SearchChangedEventName.ts`.

The `search data` refers to the object that is sent to
`/api/v2/estates/get/many`.

> Because there are many components that alter the searching data the easiest
> way of signaling a change would be through events that are catched inside the
> search page component.

The search page is listening for the event, but also all the components that
dispatch the search event are listening to the event. These components are
listening to the event because they may receive the data from that is set in
the URL.

The data sent in the event have the following structure:

```javascript
{
    // src/data/SearchEventDetail.ts
    from: string;
    urlName: string;
    fieldName: string;
    data: any;
}
```

The field `from` holds a unique ID of the component that sent the event. This
way the filter component will be able to rejects the events that come from
himself.
The `urlName` is the name that will be put in the query string for the URL.
The field `fieldName` is holding the name of the field that is put in the
search data and the field `data` is the data of the given `fieldName`.

The `urlName` is also usefull when the search page is dispatching events
parsed from the URL query for the components to know if the events are applying
to them or not.

If the `data` is null then that means that search field must be deleted from the
search data.

The number of estates that are retrieved from the server is defined in
`src/data/search_page/EstatesPerPage`.

Each time the search data is altered the following actions happen:

-   the URL should be updated;
-   a request is sent to the `portal's service` requesting the new estates;
-   while the request has not responded there are displayed skeleton loading
    estates;
-   the pagination is updated (calling to `/api/v2` to count the estates);

If the response is an empty array instead of the estates component is displayed
a message saying there are no estates found.

The following components are sending the search events:

-   the filter components (see [#Filtering system]);
-   the sort components (see [#Sort options]);
-   the locations component (see [#Locations]);
-   the pagination component (see [#Pagination]).

## Filtering system

All the filter components are using the [#Search system] for signaling the
changes.

The filters are displayed in 2 groups:

-   the primary filter characteristics that are always visible;
-   the other filter characteristics that are displayed/hidden by the switch of
    a button.

The primary filter characteristics are retrieved from
`src/data/search_data/PrimaryFilterCharacteristics.ts` which contains an array
of objects. Each object in the array follows the next structure:

```javascript
{
    label: string;
    urlName: string;
    type: number;
    fieldName: string;
    data: any;
}
```

The `label` field is for the displayed string as label using the localization
system.
The `fieldName` is the name of the field that is used in the search data.

The `urlParam` field is the key name that will be used as the query param for
the URL, also required by the search event system.

The `type` defines the type of the filter. There are 2 types of filters that
defined in `src/data/search_data/FilterTypes.ts`:

-   **multi-select filter** where the options are displayed in a select-like
    list and the user can select multiple items/values (defined as `MULTI_SELECT`);
-   **range filter** where the options are displayed as 2 select-like lists:
    `from` list and `to` list (defined as `RANGE`).

The `data` is an array of object, each object following this structure:

```javascript
{
    label: string;
    lower?: { [key: string]: any };
    upper?: { [key: string]: any };
    single: { [key: string]: any };
}
```

The `label` field is the key of the locale label of the option.
The `single` field is when only that option is selected (for `RANGE` when both
lists have the same option selected).
The `lower` field is when the selected option is the lowest selected index for
`MULTI_SELECT` or is the `from` list selected option for `RANGE`.
The `upper` field is when the selected option is the highest selected index for
`MULTI_SELECT` or is the `to` list selected option for `RANGE`.

The data sent to the search system is a combination of:

-   for `MULTI_SELECT` filter component: the `lower` field of the lowest selected
    index and the `upper` of the highest selected index, or the `single` field if
    only one option is selected;
-   for `RANGE` filter component: the `lower` of the `from` list and the `upper`
    of the `to` list, or the `single` field if both the `from` and `to` list have
    the same option selected.

The other filter characteristics are found in
`src/data/search_data/OtherFilterCharacteristics.ts` and follows the same
structure as above.

## Locations

The locations component is formed of:

-   the top component that has the county's tag and the city's tag and an input
    that allow the user to select neighborhoods;
-   the bottom component that holds the tags of selected neighborhoods;

The user cannot change/delete the county tag, nor the city tag. They can only
delete the neighborhoods tags or add new neighborhood though the input.

If there is no neighborhood selected then the bottom component containing all
the selected neighborhoods tags is hidden.

Because we have all the neighborhoods of the selected city (see
[#Initialization]) we can perform fast searches.

When a user is clicking the input it opens a list of all the neighborhoods of
the city. While the user is typing there are displayed only the neighborhoods
that starts with what the user typed.

If there is no match for what the user typed then a message is displayed
telling the user that there are no neighborhood starting with what the user
typed.

If the city does not have neighborhoods then when the user click on the input
to add more neighborhoods then a message is displayed that there are no
neighborhoods for the selected city.

## Sort options

The sort options are stored in
`src/data/search_page/sort_component/{estateCategory}.ts` where
`{estateCategory}` is the estate category as defined in `EstateCategories` of
`@softprovider/core-reactjs-items`. The content of each file is an an
array of objects, where the objects are of the following form:

```javascript
{ // src/data/search_page/SortItem.ts
    label: string;
    data: { [key: string]: 1 | -1 };
}
```

The `label` is used for getting the locale label and the `data` is the search
data that is sent to the search request.

When dispatching events for search system the `fieldName` is `sort` so the
search system know where to put the data.

The URL query param for sort is `sort` and as the value is the index of the
selected sort index if different from `0`, where `0` is the default sorting
option.

## Estates list

The documentation for the estates list can be found at
`src/docs/res/EstatesList.md`.

## Pagination

The pagination is responsible for dividing all the estates for a given search
parameters into multiple pages.

The numbers of displayed estates per page is difined in
`src/data/search_page/EstatesPerPage`.

We are only requesting the now displayed estates, and not the whole list, using
the `startFrom` and `limit` request parameters.

## Breadcrumbs

The breadcrumbs consists of the following items:

-   item representing the home page consisting of an icon and the link href is
    `/`;
-   item representing the search page with the county and city location as the URL
    (`/cauta/{urlTransactionType}/{urCategory}/{countyEncodedName}/{cityEncodedName}/{pageIndex}`);
-   if neighborhood is present in the URL then item representing the search page
    with the URL also containing the neighborhood
    (`/cauta/{urlTransactionType}/{urCategory}/{countyEncodedName}/{cityEncodedName}/{neighborhoodEncodedName}/{pageIndex}`).

[#initialization]: #initialization
[#pagination]: #pagination
[#filtering system]: #filtering-system
[#sort options]: #sort-options
[#locations]: #locations
[#search system]: #search-system

